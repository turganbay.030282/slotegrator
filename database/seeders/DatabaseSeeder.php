<?php

namespace Database\Seeders;

use App\Models\Setting;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::query()->create([
            'name' => 'John',
            'email' => 'john@gmail.com',
            'password' => bcrypt('123456'),
        ]);

        \App\Models\Present::query()->insert([
            ['name' => 'iPhone 12'],
            ['name' => 'IPad 10'],
            ['name' => 'Samsung 15'],
            ['name' => 'Nokia 3310'],
            ['name' => 'Xiaomi Redmi'],
        ]);
        \App\Models\Setting::query()->insert(
            [
                ['name' => 'Денежный приз от', 'alias' => Setting::FROM_MONEY_PRIZE_INTERVAL, 'value' => 1000],
                ['name' => 'Денежный приз до', 'alias' => Setting::TO_MONEY_PRIZE_INTERVAL, 'value' => 10000],
                ['name' => 'Бонусный приз от', 'alias' => Setting::FROM_BONUS_PRIZE_INTERVAL, 'value' => 10000],
                ['name' => 'Бонусный приз до', 'alias' => Setting::TO_BONUS_PRIZE_INTERVAL, 'value' => 100000],
                ['name' => 'Коэффициент конвертации денег в баллы', 'alias' => Setting::MONEY_CONVERSION_RATIO, 'value' => 2],
                ['name' => 'Лимит денежных призов', 'alias' => Setting::LIMIT_MONEY_PRIZE, 'value' => 100000],
                ['name' => 'Лимит призовых предметов', 'alias' => Setting::LIMIT_PRESENT_PRIZE, 'value' => 10],
            ]
        );
    }
}
