<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    const FROM_MONEY_PRIZE_INTERVAL = 'from_money_prize';
    const TO_MONEY_PRIZE_INTERVAL = 'to_money_prize';
    const FROM_BONUS_PRIZE_INTERVAL = 'from_bonus_prize';
    const TO_BONUS_PRIZE_INTERVAL = 'to_bonus_prize';
    const MONEY_CONVERSION_RATIO = 'money_to_points_conversion_ratio';
    const LIMIT_MONEY_PRIZE = 'limit_money_prize';
    const LIMIT_PRESENT_PRIZE = 'limit_present_prize';

    protected $table = 'settings';

    protected $fillable = [
        'name', 'alias', 'value'
    ];

    public function scopeFromMoney($query)
    {
        return $query->where('alias', self::FROM_MONEY_PRIZE_INTERVAL);
    }
    public function scopeToMoney($query)
    {
        return $query->where('alias', self::TO_MONEY_PRIZE_INTERVAL);
    }

    public function scopeFromBonus($query)
    {
        return $query->where('alias', self::FROM_BONUS_PRIZE_INTERVAL);
    }
    public function scopeToBonus($query)
    {
        return $query->where('alias', self::TO_BONUS_PRIZE_INTERVAL);
    }
    public function scopeConversionRatio($query)
    {
        return $query->where('alias', self::MONEY_CONVERSION_RATIO);
    }
    public function scopeMoneyLimit($query)
    {
        return $query->where('alias', self::LIMIT_MONEY_PRIZE);
    }
    public function scopePresentLimit($query)
    {
        return $query->where('alias', self::LIMIT_PRESENT_PRIZE);
    }
}
