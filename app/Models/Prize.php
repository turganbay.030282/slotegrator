<?php

namespace App\Models;

use App\Prizes\Bonus;
use App\Prizes\Money;
use App\Prizes\Present;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prize extends Model
{
    use HasFactory;

    const NEW_STATUS = 'new';
    const CANCELED_STATUS = 'canceled';
    const TRANSFERRED_STATUS = 'transferred';

    protected $table = 'prizes';

    protected $fillable = [
        'type', 'quantity', 'user_id', 'present_id', 'status'
    ];

    public function present()
    {
        return $this->belongsTo(\App\Models\Present::class, 'present_id');
    }

    public static function generateRandomPrize()
    {
        $prizes = [
            new Money(),
            new Bonus(),
            new Present()
        ];

        $randPrizeKey = array_rand($prizes);
        $prize = $prizes[$randPrizeKey];

        $data = $prize->getRandomPrize();
        if (isset($data['message'])) {
            return $data;
        }
        $data['user_id'] = auth()->id();
        return self::query()->create($data);
    }

    public static function transfer($prizeId)
    {
        $prize = self::query()
            ->where('status', 'new')
            ->find($prizeId);

        switch ($prize->type) {
            case Money::TYPE:
                $prizeType = new Money();
                break;
            case Bonus::TYPE:
                $prizeType = new Bonus();
                break;
            case Present::TYPE:
                $prizeType = new Present();
                break;
            default:
                throw new Exception('not found type prize');
        }
        $result = $prizeType->transfer($prize, request()->get('action'));
        if ($result['status']) {
            $prize->status = 'transferred';
            $prize->save();
        }

        return $result;
    }

    public static function scopeGetTotalByUser($query, $type)
    {
        $query->where('type', $type)
            ->where('user_id', auth()->id())
            ->whereDate('created_at', now());
    }
}
