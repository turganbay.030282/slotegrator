<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Loyalty extends Model
{
    use HasFactory;

    protected $table = 'loyalties';

    protected $fillable = [
        'user_id', 'count'
    ];

    public static function addUserLoyaltyCount($count, $userId)
    {
        $loyalty = self::query()
            ->where('user_id', $userId)
            ->first();

        if ($loyalty) {
            $loyalty->count += $count;
            $loyalty->save();
        } else {
            $loyalty = self::query()->create([
                'user_id' => $userId,
                'count' => $count
            ]);
        }
        return $loyalty;
    }

    public static function conversionMoney($prize): array
    {
        $conversionRatioSetting = Setting::conversionRatio()->first();
        $count = $prize->quantity * $conversionRatioSetting->value;
        return [
            'status' => !!self::addUserLoyaltyCount($count, $prize->user_id),
            'message' => 'Деньги в сумме '.$prize->quantity.' сконвертированы в баллы лояльности в приложении!'
        ];
    }
}
