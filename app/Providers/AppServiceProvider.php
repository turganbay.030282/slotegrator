<?php

namespace App\Providers;

use App\ApiBank\TransferToBank;
use App\ApiBank\TransferToBankContract;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(TransferToBankContract::class, function ($app) {
            if (request()->has('currency')) {
                return new TransferToBank('RUS');
            }
            return new TransferToBank('USD');
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
