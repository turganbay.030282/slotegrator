<?php

namespace App\Http\Controllers;

use App\Models\Prize;

class PrizeController extends Controller
{
    public function randPrize()
    {
        $prize = Prize::generateRandomPrize();
        if (isset($prize['message'])){
            return redirect()->route('dashboard')->with('message', $prize['message']);
        }
        return redirect()->route('prize.show', ['prizeId' => $prize->id]);
    }

    public function show($prizeId)
    {
        return view('prize.show', [
            'prize' => Prize::with('present')->findOrFail($prizeId),
        ]);
    }

    public function transfer($prizeId)
    {
        $result = Prize::transfer($prizeId);
        return redirect()->route('dashboard')->with('message', $result['message']);
    }
}
