<?php


namespace App\Prizes;


interface IPrize
{
    public function getRandomPrize();
    public function transfer($prize, $action);
}
