<?php


namespace App\Prizes;


use App\Models\Loyalty;
use App\Models\Setting;

class Bonus implements IPrize
{
    const TYPE = 'bonus';

    public function getRandomPrize()
    {
        $fromBonusSetting = Setting::fromBonus()->first();
        $toBonusSetting = Setting::toBonus()->first();
        return [
            'type' => self::TYPE,
            'quantity' => rand($fromBonusSetting->value, $toBonusSetting->value),
        ];
    }

    public function transfer($prize, $action)
    {
        return [
            'status' => !!Loyalty::addUserLoyaltyCount($prize->quantity, $prize->user_id),
            'message' => 'Бонусные баллы зачислены на счет лояльности в приложении!'
        ];
    }
}
