<?php


namespace App\Prizes;


use App\Models\Prize;
use App\Models\Setting;

class Present implements IPrize
{
    const TYPE = 'present';

    public function getRandomPrize()
    {
        $totalByUser = Prize::getTotalByUser(self::TYPE)->count();
        $presentLimitSetting = Setting::presentLimit()->first();

        if ($totalByUser >= $presentLimitSetting->value) {
            return ['message' => "Предмет превысил лимит!"];
        }

        $present = \App\Models\Present::query()
            ->where('active', true)
            ->inRandomOrder()
            ->first();

        return [
            'type' => self::TYPE,
            'present_id' => $present->id
        ];
    }

    public function transfer($prize, $action)
    {
        if ($action == 'to-mail') {
            //todo mail sending logic
            return [
                'status' => 1,
                'message' => $prize->present->name.' отправлен по почте (вручную работником)!'
            ];
        } else {
            return [
                'status' => 1,
                'message' => $prize->present->name. ' отменен!'
            ];
        }
    }

}
