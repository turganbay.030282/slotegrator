<?php


namespace App\Prizes;


use App\ApiBank\TransferToBankContract;
use App\Models\Loyalty;
use App\Models\Prize;
use App\Models\Setting;

class Money implements IPrize
{
    const TYPE = 'money';

    public function getRandomPrize()
    {
        $fromMoneySetting = Setting::fromMoney()->first();
        $toMoneySetting = Setting::toMoney()->first();
        $quantity = rand($fromMoneySetting->value, $toMoneySetting->value);

        $totalByUser = Prize::getTotalByUser(self::TYPE)->sum('quantity');
        $moneyLimitSetting = Setting::moneyLimit()->first();
        $totalByUser += $quantity;
        if ($totalByUser >= $moneyLimitSetting->value) {
            return ['message' => "Денежный приз превысил лимит!"];
        }
        return [
            'type' => self::TYPE,
            'quantity' => $quantity
        ];
    }

    public function transfer($prize, $action)
    {
        if ($action == 'to-bank') {
            $transferToBank = app()->get(TransferToBankContract::class);
            return $transferToBank->charge($prize->quantity);
        }

        if ($action == 'to-loyalty') {
            return Loyalty::conversionMoney($prize);
        }
    }
}
