<?php


namespace App\ApiBank;

interface TransferToBankContract
{
    public function charge($amount);
}
