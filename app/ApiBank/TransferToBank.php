<?php


namespace App\ApiBank;


use Illuminate\Support\Str;

class TransferToBank implements TransferToBankContract
{

    const FAIL_STATUS = 0;
    const SUCCESS_STATUS = 1;

    private $currency;

    public function __construct($currency)
    {
        $this->currency = $currency;
    }

    public function charge($amount): array
    {
        try {
            //todo API sending money to user's account
            return [
                'status' => self::SUCCESS_STATUS,
                'message' => "Деньги $amount ({$this->currency}) перечислены на счет в банке!"
            ];
        } catch (\Exception $exception){
            return [
                'status' => self::FAIL_STATUS,
                'message' => $exception->getMessage()
            ];
        }
    }
}
