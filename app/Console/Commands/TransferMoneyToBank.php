<?php

namespace App\Console\Commands;

use App\ApiBank\TransferToBankContract;
use App\Models\Prize;
use App\Models\User;
use App\Prizes\Money;
use Illuminate\Console\Command;

class TransferMoneyToBank extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transfer:money {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Transfer money prize to bank';
    /**
     * @var TransferToBankContract
     */
    private $bank;


    public function __construct(TransferToBankContract $bank)
    {
        parent::__construct();
        $this->bank = $bank;
    }


    public function handle()
    {
        $moneyPrizes = Prize::query()
            ->where('user_id', $this->argument('userId'))
            ->where('type', Money::TYPE)
            ->where('status', Prize::NEW_STATUS)
            ->get();

        if ($moneyPrizes->count()){
           $result = $this->bank->charge($moneyPrizes->sum('quantity'));
           if ($result['status']) {
               foreach ($moneyPrizes as $prize){
                   $prize->status = Prize::TRANSFERRED_STATUS;
                   $prize->save();
               }
               $this->info($result['message']);
           } else {
               $this->error($result['message']);
           }
        } else {
            $this->error('Не имеется денег для перевода');
        }
    }
}
