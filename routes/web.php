<?php

use App\Http\Controllers\PrizeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome');
Route::group(['prefix' => '/', 'middleware' => ['auth:sanctum', 'verified']], function () {
    Route::view('dashboard', 'dashboard')->name('dashboard');
    Route::group(['prefix' => 'prize'], function () {
        Route::get('rand', [PrizeController::class, 'randPrize'])->name('prize.rand');
        Route::get('{prizeId}/show', [PrizeController::class, 'show'])->name('prize.show');
        Route::get('{prizeId}/transfer', [PrizeController::class, 'transfer'])->name('prize.transfer');
    });
});
