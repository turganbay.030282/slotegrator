<?php

namespace Tests\Feature;

use App\Models\Loyalty;
use App\Models\Prize;
use App\Models\Setting;
use App\Prizes\Money;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ConversionMoneyTest extends TestCase
{
    use RefreshDatabase;

    public function testConversionMoney()
    {
        $userId = 1;
        $this->assertDatabaseHas('users', [
            'id' => $userId,
            'name' => 'John',
            'email' => 'john@gmail.com',
        ]);
        $this->assertDatabaseHas('settings', [
            'alias' => Setting::MONEY_CONVERSION_RATIO,
        ]);

        $prize = Prize::query()->create([
            'type' => Money::TYPE,
            'quantity' => 1000,
            'user_id' => $userId,
            'status' => Prize::NEW_STATUS
        ]);

        $result = Loyalty::conversionMoney($prize);
        $this->assertTrue($result['status']);

        $userLoyalty = Loyalty::query()->where('user_id', $userId)->first();

        $conversionRatioSetting = Setting::conversionRatio()->first();
        $this->assertEquals($prize->quantity, $userLoyalty->count / $conversionRatioSetting->value);
    }
}
