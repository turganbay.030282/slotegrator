<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight px-10">
            @if($prize->type == 'money')
                Деньги: {{$prize->quantity}}
            @elseif($prize->type == 'bonus')
                Бонусы: {{$prize->quantity}}
            @elseif($prize->type == 'present')
                Предмет: {{$prize->present->name}}
            @endif
        </h2>
    </x-slot>

    <div class="py-12">
        @if($prize->status == \App\Models\Prize::NEW_STATUS)
            @if($prize->type == \App\Prizes\Money::TYPE)
                <div class="flex max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div class="w-64 m-5 bg-white overflow-hidden shadow-xl sm:rounded-lg">
                        <a href="{{route('prize.transfer', ['prizeId' => $prize->id, 'action' => 'to-bank'])}}"
                           class="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 md:py-4 md:text-lg md:px-10">
                            Перечислить на счет в банке
                        </a>
                    </div>
                    <div class="w-64 m-5 bg-white overflow-hidden shadow-xl sm:rounded-lg">
                        <a href="{{route('prize.transfer', ['prizeId' => $prize->id, 'action' => 'to-loyalty'])}}"
                           class="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 md:py-4 md:text-lg md:px-10">
                            Конвертировать в баллы лояльности
                        </a>
                    </div>
                </div>
            @elseif($prize->type == \App\Prizes\Bonus::TYPE)
                <div class="flex max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div class="w-64 m-5 bg-white overflow-hidden shadow-xl sm:rounded-lg">
                        <a href="{{route('prize.transfer', ['prizeId' => $prize->id])}}"
                           class="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 md:py-4 md:text-lg md:px-10">
                            Зачислить на счет лояльности
                        </a>
                    </div>
                </div>
            @elseif($prize->type == \App\Prizes\Present::TYPE)
                <div class="flex max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div class="w-64 m-5 bg-white overflow-hidden shadow-xl sm:rounded-lg">
                        <a href="{{route('prize.transfer', ['prizeId' => $prize->id, 'action' => 'to-mail'])}}"
                           class="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 md:py-4 md:text-lg md:px-10">
                            Получить
                        </a>
                    </div>
                    <div class="w-64 m-5 bg-white overflow-hidden shadow-xl sm:rounded-lg">
                        <a href="{{route('prize.transfer', ['prizeId' => $prize->id, 'action' => 'cancel'])}}"
                           class="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 md:py-4 md:text-lg md:px-10">
                            Отказаться
                        </a>
                    </div>
                </div>
            @endif
        @else
            <div class="flex max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="w-64 m-5 bg-white overflow-hidden shadow-xl sm:rounded-lg">
                    <a href="{{url()->previous()}}"
                       class="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 md:py-4 md:text-lg md:px-10">
                        Назад
                    </a>
                </div>
            </div>
        @endif

    </div>
</x-app-layout>
